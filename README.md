Fuse FontAwesome
==============

Module to use [FontAwesome](http://fontawesome.io/) in [Fuse](http://www.fusetools.com/).

## Installation

Using [fusepm](https://github.com/bolav/fusepm)

    $ fusepm install https://github.com/danmademe/fuse-fontawesome


## Usage:
  Go to http://fontawesome.io add find the icon you want to use, so for instance if the icon is `fa-check`, mark it up like so
  ```<fa-check />```

  If you want to pass Color into it you can do it like so

  ```<fa-check Color="#000" />```
