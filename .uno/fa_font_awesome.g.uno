[Uno.Compiler.UxGenerated]
public partial class fa_font_awesome: Fuse.Controls.Text
{
    static fa_font_awesome()
    {
    }
    [global::Uno.UX.UXConstructor]
    public fa_font_awesome()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this.Value = "\uF2B4";
        this.Font = FontAwesome.AwesomeFont;
    }
}
