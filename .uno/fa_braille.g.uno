[Uno.Compiler.UxGenerated]
public partial class fa_braille: Fuse.Controls.Text
{
    static fa_braille()
    {
    }
    [global::Uno.UX.UXConstructor]
    public fa_braille()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this.Value = "\uF2A1";
        this.Font = FontAwesome.AwesomeFont;
    }
}
