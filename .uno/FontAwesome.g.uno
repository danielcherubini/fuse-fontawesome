[Uno.Compiler.UxGenerated]
public partial class FontAwesome: Fuse.App
{
    [global::Uno.UX.UXGlobalResource("AwesomeFont")] public static readonly Fuse.Font AwesomeFont;
    static FontAwesome()
    {
        AwesomeFont = new Fuse.Font(new global::Uno.UX.BundleFileSource(import global::Uno.IO.BundleFile("../font.otf")));
        global::Uno.UX.Resource.SetGlobalKey(AwesomeFont, "AwesomeFont");
    }
    [global::Uno.UX.UXConstructor]
    public FontAwesome()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new Fuse.Reactive.FuseJS.TimerModule();
        var temp1 = new Fuse.Reactive.FuseJS.Http();
        var temp2 = new Fuse.Storage.StorageModule();
    }
}
