[Uno.Compiler.UxGenerated]
public partial class fa_sign_language: Fuse.Controls.Text
{
    static fa_sign_language()
    {
    }
    [global::Uno.UX.UXConstructor]
    public fa_sign_language()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this.Value = "\uF2A7";
        this.Font = FontAwesome.AwesomeFont;
    }
}
