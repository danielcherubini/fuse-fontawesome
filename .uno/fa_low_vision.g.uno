[Uno.Compiler.UxGenerated]
public partial class fa_low_vision: Fuse.Controls.Text
{
    static fa_low_vision()
    {
    }
    [global::Uno.UX.UXConstructor]
    public fa_low_vision()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this.Value = "\uF2A8";
        this.Font = FontAwesome.AwesomeFont;
    }
}
